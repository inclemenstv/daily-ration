//
//  Coordinator.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 22.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation


protocol Coordinator {
    func start()
}
