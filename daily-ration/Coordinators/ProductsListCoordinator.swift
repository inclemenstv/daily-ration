//
//  ProductsListCoordinator.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 22.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import UIKit


class ProductsListCoordinator: Coordinator {
    
    private let presenter: UINavigationController
    private var productsListViewController: ProductsListViewController?
    private var detailProductCoordinator: DetailProductCoordinator?

    
    init(presenter: UINavigationController) {
        self.presenter = presenter
        
        }
        
   
    
    func start() {
        let productsListViewController = ProductsListViewController(nibName: nil, bundle: nil)
        presenter.pushViewController(productsListViewController, animated: true)
        productsListViewController.title = "Products List"
        self.productsListViewController = productsListViewController
        productsListViewController.delegate = self

    }
    }
    


extension ProductsListCoordinator: ProductsListViewControllerDelegate {
    func productsListViewControllerDidSelectProduct(_ selectedProduct: Products) {
        
        let detailProductCoordinator = DetailProductCoordinator(presenter: presenter, product: selectedProduct)
        detailProductCoordinator.start()
        
        self.detailProductCoordinator = detailProductCoordinator
        print(selectedProduct)
    }
    
    
}
    
    

