//
//  AppCoordinator.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 22.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import UIKit


class AppCoordinator: Coordinator {
    
    let firebaseService: FirebaseService
    let window: UIWindow
    let rootViewController: UINavigationController
    let productsListCoordinator: ProductsListCoordinator

    
    init(window: UIWindow) {
        self.window = window
        firebaseService = FirebaseService()
        rootViewController = UINavigationController()

        productsListCoordinator = ProductsListCoordinator(presenter: rootViewController)
        
    }
    
    func start() {
        window.rootViewController = rootViewController
        
        productsListCoordinator.start()
        window.makeKeyAndVisible()
    }
    
}
