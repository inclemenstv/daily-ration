//
//  DetailProductCoordinator.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 25.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import UIKit

class DetailProductCoordinator: Coordinator {
   
    private let presenter: UINavigationController
    private var detailProductViewController: DetailProductViewController?
    private let product: Products
    
    init(presenter: UINavigationController, product: Products) {
        self.presenter = presenter
        self.product = product
        print("detail---\(product)")
 
    }
        
    func start() {
           
        let detailProductViewController = DetailProductViewController(product: product, nibName: nil, bundle: nil)
        
        detailProductViewController.title = "Detail Product"
        presenter.pushViewController(detailProductViewController, animated: true)
        self.detailProductViewController = detailProductViewController
        
       }
}
