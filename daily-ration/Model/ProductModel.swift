//
//  ProductModel.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 24.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import Firebase




struct Products {
    
    var productsId: String
    var call: Double
    var name: String
    var carbo: Double
    var protein: Double
    var fat: Double
   
    
    init?(productsId: String, dictionary: [String: Any]) {
        self.productsId = productsId
        
        guard let name = dictionary["Name"] as? String,
            let call = dictionary["Call"] as? Double,
            let carbo = dictionary["Carbo"] as? Double,
            let protein = dictionary["Protein"] as? Double,
            let fat = dictionary["Fat"] as? Double
            else {return nil}
        
        self.call = call
        self.name = name
        self.carbo = carbo
        self.protein = protein
        self.fat = fat
    }
    
}



