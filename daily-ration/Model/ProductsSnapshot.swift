//
//  ProductsSnapshot.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 24.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import Firebase


struct ProductsSnapshot {
    
    var products: [Products]
    
    init?(with snapshot: DataSnapshot) {
        var products = [Products]()
        
        guard let snapDict = snapshot.value as? [String: [String: Any]] else { return nil }
        
        for snap in snapDict {
           guard let product = Products(productsId: snap.key, dictionary: snap.value) else { continue }
            products.append(product)
        }
        self.products = products
        
    }
}
