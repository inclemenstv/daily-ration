//
//  ProductsListViewController.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 22.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import UIKit
import Firebase


protocol ProductsListViewControllerDelegate: class {
    
    func productsListViewControllerDidSelectProduct(_ selectedProduct: Products )
}



class ProductsListViewController: UIViewController {
    
    
    //MARK: - Variable
    weak var delegate: ProductsListViewControllerDelegate?
    var productsArray = [Products]()
    let firebaseService = FirebaseService()
    
    
    
    
    //MARK: - Outlet
    @IBOutlet weak var productsListTableView: UITableView! {
        didSet {
            productsListTableView.dataSource = self
            productsListTableView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        productsListTableView.tableFooterView = UIView()
        
        navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .add, target: self, action: #selector(addNewProduct(_:)))
    }
    
    
    //MARK: - Functions
    
    
    func updateUI() {
        firebaseService.getList { (productsList) in
            self.productsArray = productsList
            self.productsListTableView.reloadData()
        }
    }
    
    @objc func addNewProduct(_ sender: UIBarButtonItem) {
        
        var product = [String: Any]()
        
        
        let alertController = UIAlertController(title: "New products", message: "add new products", preferredStyle: .alert)
        
        alertController.addTextField { (tFName) in
            tFName.placeholder = "Enter named"
        }
        alertController.addTextField { (tFCall) in
            tFCall.placeholder = "Enter call"
            tFCall.keyboardType = .numberPad
        }
        alertController.addTextField { (tFCarbo) in
            tFCarbo.placeholder = "Enter carbo"
            tFCarbo.keyboardType = .numberPad
        }
        alertController.addTextField { (tFProtein) in
            tFProtein.placeholder = "Enter protein"
            tFProtein.keyboardType = .numberPad
        }
        alertController.addTextField { (tfFat) in
            tfFat.placeholder = "Enter fat"
            tfFat.keyboardType = .numberPad
        }
        
        let action = UIAlertAction(title: "ok", style: .default) { (action) in
            guard let name = alertController.textFields?.first?.text else {
                self.errorAlert()
                return
            }
            guard let call = Double(alertController.textFields![1].text!) else {
                self.errorAlert()
                return
            }
            guard  let carbo = Double(alertController.textFields![2].text!) else {
                self.errorAlert()
                return
            }
            guard let protein = Double(alertController.textFields![3].text!) else {
                self.errorAlert()
                return
                
            }
            guard let fat = Double(alertController.textFields![4].text!)
                else {
                    self.errorAlert()
                    return
            }
            product = ["Name": name, "Call": call, "Carbo": carbo, "Protein": protein, "Fat": fat]
            
            self.firebaseService.saveToFirebase(products: product)

        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func errorAlert() {
        
        let alert = UIAlertController(title: "Error", message: "enter all value", preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        alert.addAction(actionCancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}


