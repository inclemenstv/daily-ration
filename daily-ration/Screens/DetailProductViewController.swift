//
//  DetailProductViewController.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 25.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class DetailProductViewController: UIViewController {


    @IBOutlet weak var nameLb: UILabel!
    
    var product: Products
    @IBOutlet weak var callLb: UILabel!
    @IBOutlet weak var carboLb: UILabel!
    @IBOutlet weak var fatLb: UILabel!
    
    @IBOutlet weak var proteinLb: UILabel!
    init(product: Products, nibName: String?, bundle: Bundle?) {
        self.product = product
        super.init(nibName: nibName, bundle: bundle)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
      

        print("Its me - \(product)")
        nameLb.text = product.name
        callLb.text = "Callories: \(product.call) per 100g"
        carboLb.text = "Carbo: \(product.carbo) per 100g"
        proteinLb.text = "Protein: \(product.protein) per 100g"
        fatLb.text = "Fat: \(product.fat) per 100g"
        
        
    }
    
   

}
