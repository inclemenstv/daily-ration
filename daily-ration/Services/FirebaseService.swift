//
//  FirebaseService.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 24.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import Firebase

struct ProductCache {
    let productsArray: [Products]
//    let snapshot: ProductsSnapshot
}

class FirebaseService {
    
     let productsReference = Database.database().reference().child("Products")

      init() {}

    func getList(completion: @escaping([Products]) -> () ) {
            
       productsReference.observe(.value) { (snapshot) in
             guard let productsSnapshot1 = ProductsSnapshot(with: snapshot) else { return }
            let product = productsSnapshot1.products
            DispatchQueue.main.async {
                               completion(product)
                           }
                    
        }
    }
 
    func saveToFirebase(products: [String: Any]) {
        
        let key = productsReference.childByAutoId()
            
        key.setValue(products)
    }
    
    
}

   


    

    

    
        
    
    
    
    
    
        
        
    
        

