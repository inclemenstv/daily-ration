//
//  ProductsListTableViewControllerExtension.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 24.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import UIKit


extension ProductsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return productsArray.count
//        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell: UITableViewCell
//        if let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: "ListItem") {
//             cell = dequeuedCell
//           } else {
//             cell = UITableViewCell(style: .subtitle, reuseIdentifier: "ListItem")
//           }
//
//        cell.textLabel?.text = productsArray[indexPath.row].name
//
//        return cell
        
//        let product = productList[indexPath.row]
        let product = productsArray[indexPath.row]
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)      
        cell.textLabel?.text = product.name
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let product = productsArray[indexPath.row]
//        let product = productList[indexPath.row]
        delegate?.productsListViewControllerDidSelectProduct(product)
        tableView.deselectRow(at: indexPath, animated: true)
        

        
        
    }
    
    
}


