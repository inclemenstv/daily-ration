//
//  AppDelegate.swift
//  daily-ration
//
//  Created by Konstantin Pischanskyi on 22.01.2020.
//  Copyright © 2020 Konstantin Pischanskyi. All rights reserved.
//

import UIKit
import Firebase




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  private var applicationCoordinator: AppCoordinator?  // 1
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions:
                     [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    FirebaseApp.configure()


    let window = UIWindow(frame: UIScreen.main.bounds)
    let applicationCoordinator = AppCoordinator(window: window) // 2

    
    self.window = window
    self.applicationCoordinator = applicationCoordinator
    
    applicationCoordinator.start()  // 3
    

    
    return true
  }
}


